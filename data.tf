## data source is used to fetch the information of the existing resource.

data "aws_vpc" "default" {
  id = var.DEFAULT_VPC_ID
}