resource "aws_route_table" "public" {
  vpc_id = aws_vpc.main.id

  tags = {
    Name = "${var.PROJECTNAME}-public-rt"
  }
}


resource "aws_route" "public-rt" {
  route_table_id            =  aws_route_table.public.id
  destination_cidr_block    = "0.0.0.0/0"
  gateway_id                = aws_internet_gateway.igw.id
  depends_on                = [aws_route_table.public]
}

## Adding Route of the peering connection to the public route table

resource "aws_route" "peering-rt" {
  route_table_id                         =  aws_route_table.public.id
  destination_cidr_block                 = "172.31.0.0/16"
  vpc_peering_connection_id               = aws_vpc_peering_connection.foo.id
  depends_on                             = [aws_route_table.public]
}


  

# creating the route table for private subnet

resource "aws_route_table" "private" {
  vpc_id = aws_vpc.main.id

  tags = {
    Name = "${var.PROJECTNAME}-private-rt"
  }
}

# adding the route to private subnet

resource "aws_route" "private-rt" {
  route_table_id                =  aws_route_table.private.id
  destination_cidr_block        = "0.0.0.0/0"
  nat_gateway_id                = aws_nat_gateway.ngw.id
  depends_on                    = [aws_route_table.private]
}

# Adding the route table to public subnet

resource "aws_route_table_association" "public" {
  count          = length(aws_subnet.public.*.id)
  subnet_id      = element(aws_subnet.public.*.id,count.index)
  route_table_id = aws_route_table.public.id
}


# Adding the raoute table to private subnet

resource "aws_route_table_association" "private" {
  count          = length(aws_subnet.private.*.id)
  subnet_id      = element(aws_subnet.private.*.id,count.index)
  route_table_id = aws_route_table.private.id
}